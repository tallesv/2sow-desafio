import React from 'react';
import { Link } from 'react-router-dom';

import { useAuth } from '../../hooks/auth';

import { Container } from './style';

const NavBar: React.FC = () => {
  const { user, signOut } = useAuth();
  return (
    <Container>
      <div>
        <h1>User App</h1>
      </div>

      {user && (
        <div className="right-navbar-menu">
          <div>
            <Link to="/createUser">
              <span>Cadastrar</span>
            </Link>
            <Link to="/">
              <span>Usuários</span>
            </Link>
          </div>
          <div className="user-info">
            <h3>{user.nome}</h3>
            <span onClickCapture={signOut}>Sair</span>
          </div>
        </div>
      )}
    </Container>
  );
};

export default NavBar;
