import styled from 'styled-components';
import { shade } from 'polished';

export const Container = styled.header`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;

  width: 100%;
  height: 70px;
  background: #25274d;
  color: #edf5e1;

  .right-navbar-menu {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    div {
      padding: 0 20px;

      span {
        color: #edf5e1;
        padding: 0 5px;
        cursor: pointer;

        &:hover {
          color: ${shade(0.3, '#edf5e1')};
        }
      }
    }

    .user-info {
      display: flex;
      justify-content: space-between;
      align-items: center;
      text-align: center;

      h3 {
        padding: 10px 20px 0 0;
      }
    }
  }
`;
