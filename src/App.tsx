import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import GlobalStyle from './styles/global';
import Routes from './routes';
import { AuthProvider } from './hooks/auth';
import { ToastProvider } from './hooks/toast';
import 'semantic-ui-css/semantic.min.css';

const App: React.FC = () => {
  return (
    <>
      <AuthProvider>
        <ToastProvider>
          <BrowserRouter>
            <Routes />
          </BrowserRouter>
        </ToastProvider>
      </AuthProvider>
      <GlobalStyle />
    </>
  );
};

export default App;
