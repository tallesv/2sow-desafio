import React, { useCallback, useEffect, useState } from 'react';
import { Button, Image, Input } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';
import NavBar from '../../components/NavBar';

import defaultAvatar from '../../assets/default-avatar.jpg';

import { Container, TitleAndSearch, UsersCardContent, UserCard } from './style';
import api from '../../services/api';
import { useToast } from '../../hooks/toast';

interface Endereço {
  cep: string;
  cidade: string;
  bairro: string;
  rua: string;
  numero: string;
}
interface User {
  id: string;
  nome: string;
  email: string;
  cpf: string;
  endereço: Endereço;
}

const Dashboard: React.FC = () => {
  const history = useHistory();
  const { addToast } = useToast();
  const [users, setUsers] = useState<User[]>([]);
  const [loading, setLoading] = useState(true);

  const handleFindUser = useCallback(
    (userName: string) => {
      if (userName !== '') {
        const usersToFind = users.filter(user => user.nome.includes(userName));
        setUsers(usersToFind);
      }

      if (userName === '') {
        api.get('/usuarios').then(response => setUsers(response.data));
      }
    },
    [users],
  );

  const handleDeleteUser = useCallback(
    async (userId: string) => {
      try {
        setLoading(true);
        await api.delete(`/usuarios/${userId}`);
        setUsers(users.filter(user => user.id !== userId));
        setLoading(false);
        addToast({
          type: 'success',
          title: 'Usuário excluído.',
        });
      } catch (err) {
        setLoading(false);
        addToast({
          type: 'error',
          title: 'Erro na exclusão do usuário.',
          description: 'Ocorreu um erro ao excluir o usuário, tente novamente.',
        });
      }
    },
    [addToast, users],
  );

  useEffect(() => {
    api.get('/usuarios').then(response => {
      setUsers(response.data);
      setLoading(false);
    });
  }, []);

  return (
    <>
      <NavBar />
      <Container>
        <TitleAndSearch>
          <h1>Lista de Usuários</h1>
          <Input
            icon="search"
            placeholder="Procure por um usuário"
            onChange={e => handleFindUser(e.target.value)}
          />
        </TitleAndSearch>
        <UsersCardContent>
          {loading ? (
            <div>Carregando usuários...</div>
          ) : (
            <div className="ui cards">
              {users.map(user => (
                <UserCard key={user.id}>
                  <div className="ui card">
                    <div className="content">
                      <Image
                        alt="1a"
                        src={defaultAvatar}
                        className="ui mini right floated image"
                      />
                      <div className="header">{user.nome}</div>
                      <div className="description">
                        <div className="cpf">
                          <strong>CPF: </strong>
                          {user.cpf}
                        </div>
                        <div className="email">
                          <strong>email: </strong>
                          {user.email}
                        </div>
                        <div className="cidade">
                          <strong>Cidade: </strong>
                          {user.endereço.cidade}
                        </div>
                      </div>
                    </div>
                    <div className="extra content">
                      <div className="ui two buttons">
                        <Button
                          className="ui blue basic button"
                          onClick={() => history.push('/editUser', user)}
                        >
                          Editar
                        </Button>
                        <Button
                          className="ui red basic button"
                          onClick={() => handleDeleteUser(user.id)}
                        >
                          Excluir
                        </Button>
                      </div>
                    </div>
                  </div>
                </UserCard>
              ))}
            </div>
          )}
        </UsersCardContent>
      </Container>
    </>
  );
};

export default Dashboard;
