import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 40px 80px;
`;

export const TitleAndSearch = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin-bottom: 50px;
  padding: 0 60px 0 15px;
  h1 {
    text-align: left;
  }

  @media (max-width: 818px) {
    flex-direction: column;
  }
`;

export const UsersCardContent = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  flex-wrap: wrap;
`;

export const UserCard = styled.div`
  padding: 10px 20px;

  .ui.card {
    height: 250px;

    .description {
      height: 100%;
      display: flex;
      flex-direction: column;
      justify-content: space-around;
      padding-bottom: 30px;
    }
  }
`;
