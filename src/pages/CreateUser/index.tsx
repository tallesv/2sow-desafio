/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useCallback } from 'react';
import * as Yup from 'yup';
import { Form, Button } from 'semantic-ui-react';
import MaskedInput from 'react-text-mask';
import { useFormik } from 'formik';
import { v4 as uuid } from 'uuid';
import NavBar from '../../components/NavBar';

import { Container, FormContent } from './style';
import api from '../../services/api';
import { useToast } from '../../hooks/toast';

const CreateUser: React.FC = () => {
  const { addToast } = useToast();

  const schema = Yup.object().shape({
    name: Yup.string().required('Nome obrigatório'),
    email: Yup.string()
      .email('O email informado não é valido')
      .required('Email obrigatório'),
    cpf: Yup.string().required('CPF obrigatório'),
    cep: Yup.string().required('CEP obrigatório'),
    city: Yup.string().required('Cidade obrigatória'),
    neighborhood: Yup.string().required('Bairro obrigatório'),
    street: Yup.string().required('Rua obrigatória'),
    number: Yup.string().required('Número da Rua obrigatório'),
  });

  const formik = useFormik({
    initialValues: {
      name: '',
      email: '',
      cpf: '',
      cep: '',
      city: '',
      neighborhood: '',
      street: '',
      number: '',
    },
    enableReinitialize: true,
    validationSchema: schema,
    validateOnChange: false,
    onSubmit: async formData => {
      try {
        const endereço = {
          cep: formData.cep,
          rua: formData.street,
          numero: formData.number,
          bairro: formData.neighborhood,
          cidade: formData.city,
        };

        const user = {
          id: uuid(),
          nome: formData.name,
          cpf: formData.cpf,
          email: formData.email,
          endereço,
        };
        await api.post('/usuarios', user);
        addToast({
          type: 'success',
          title: 'Usuário cadastrado.',
        });
        formik.resetForm();
      } catch (err) {
        addToast({
          type: 'error',
          title: 'Erro no cadastro do usuário.',
          description:
            'Ocorreu um erro ao cadastrar o usuário, tente novamente.',
        });
      }
    },
  });

  const handleCEP = useCallback(
    async (e: React.ChangeEvent<HTMLInputElement>) => {
      const cep = e.target.value.replace('-', '');
      formik.handleChange(e);
      let numbersQuantity = 0;
      // checa quantos numeros a string cep tem
      for (let i = 0; i < cep.length; i += 1) {
        if (/^-?[\d.]+(?:e-?\d+)?$/.test(cep[i])) {
          numbersQuantity += 1;
        }
      }

      if (numbersQuantity === 8) {
        const response = await api.get(`https://viacep.com.br/ws/${cep}/json/`);
        const cepData = response.data;
        formik.setFieldValue('city', cepData.localidade);
        formik.setFieldValue('neighborhood', cepData.bairro);
        formik.setFieldValue('street', cepData.logradouro);
      }
    },
    [formik],
  );

  return (
    <>
      <NavBar />
      <Container>
        <h1>Cadastrar Usuário</h1>

        <FormContent>
          <Form onSubmit={formik.handleSubmit}>
            <div>
              <div>
                <span>*Todos os campos são obrigatórios</span>

                <label>Nome</label>
                <Form.Input
                  name="name"
                  error={formik.errors.name}
                  onChange={formik.handleChange}
                  value={formik.values.name}
                />

                <label>Email</label>
                <Form.Input
                  name="email"
                  error={formik.errors.email}
                  onChange={formik.handleChange}
                  value={formik.values.email}
                />

                <label>CPF</label>
                <Form.Input error={formik.errors.cpf} className="cpf">
                  <MaskedInput
                    mask={[
                      /[0-9]/,
                      /\d/,
                      /\d/,
                      '.',
                      /\d/,
                      /\d/,
                      /\d/,
                      '.',
                      /\d/,
                      /\d/,
                      /\d/,
                      '-',
                      /\d/,
                      /\d/,
                    ]}
                    guide
                    name="cpf"
                    onChange={formik.handleChange}
                    value={formik.values.cpf}
                  />
                </Form.Input>

                <label>CEP</label>
                <Form.Input error={formik.errors.cep} className="cep">
                  <MaskedInput
                    mask={[
                      /[0-9]/,
                      /\d/,
                      /\d/,
                      /\d/,
                      /\d/,
                      '-',
                      /\d/,
                      /\d/,
                      /\d/,
                    ]}
                    guide
                    name="cep"
                    value={formik.values.cep}
                    onChange={e => {
                      handleCEP(e);
                    }}
                  />
                </Form.Input>

                <label>Cidade</label>
                <Form.Input
                  name="city"
                  error={formik.errors.city}
                  onChange={formik.handleChange}
                  value={formik.values.city}
                />

                <label>Bairro</label>
                <Form.Input
                  name="neighborhood"
                  error={formik.errors.neighborhood}
                  onChange={formik.handleChange}
                  value={formik.values.neighborhood}
                />

                <div className="rua-numero">
                  <div className="rua">
                    <label>Rua</label>
                    <Form.Input
                      name="street"
                      error={formik.errors.street}
                      onChange={formik.handleChange}
                      value={formik.values.street}
                      className="rua-input"
                    />
                  </div>

                  <div className="numero">
                    <label>Número</label>
                    <Form.Input
                      name="number"
                      error={formik.errors.number}
                      onChange={formik.handleChange}
                      value={formik.values.number}
                    />
                  </div>
                </div>

                <div className="form-buttons">
                  <Button
                    type="button"
                    onClick={e => {
                      formik.handleReset(e);
                    }}
                    content="Resetar dados"
                  />
                  <Button type="submit" content="Cadastrar" />
                </div>
              </div>
            </div>
          </Form>
        </FormContent>
      </Container>
    </>
  );
};

export default CreateUser;
