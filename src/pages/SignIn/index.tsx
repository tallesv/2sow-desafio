import React, { useCallback, useState } from 'react';
import * as Yup from 'yup';
import { Input, Button } from 'semantic-ui-react';

import { useHistory } from 'react-router-dom';
import NavBar from '../../components/NavBar';

import { Container, Form } from './style';
import { useAuth } from '../../hooks/auth';
import { useToast } from '../../hooks/toast';

interface Login {
  email: string;
  senha: string;
}

const SignIn: React.FC = () => {
  const { addToast } = useToast();
  const history = useHistory();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { signIn } = useAuth();

  const handleSubmit = useCallback(async () => {
    try {
      const schema = Yup.object().shape({
        email: Yup.string()
          .email('O email tem que ser valido')
          .required('Informe seu email'),
        password: Yup.string().min(
          4,
          'A senha tem que ter no mínimo 4 dígitos',
        ),
      });

      const data = {
        email,
        password,
      };

      await schema.validate(data, { abortEarly: false });
      const login = await signIn({ email: data.email, senha: data.password });
      if (login === 'error') {
        throw Error;
      }
      history.push('/');
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        err.errors.map(error =>
          addToast({
            type: 'error',
            title: 'Erro na autenticação',
            description: error,
          }),
        );
      } else {
        addToast({
          type: 'error',
          title: 'Erro na autenticação',
          description: 'Ocorreu um erro ao fazer login, cheque as cedenciais.',
        });
      }
    }
  }, [addToast, email, history, password, signIn]);

  return (
    <Container>
      <NavBar />
      <Form>
        <form onSubmit={handleSubmit}>
          <div>
            <h2>Sign In</h2>

            <Input
              name="email"
              type="email"
              value={email}
              onChange={e => setEmail(e.target.value)}
              placeholder="email"
              className="email-input"
            />

            <Input
              name="password"
              type="password"
              value={password}
              onChange={e => setPassword(e.target.value)}
              placeholder="senha"
              className="password-input"
            />

            <Button
              type="button"
              content="Entrar"
              className="signin-button"
              onClick={handleSubmit}
            />
          </div>
        </form>
      </Form>
    </Container>
  );
};

export default SignIn;
