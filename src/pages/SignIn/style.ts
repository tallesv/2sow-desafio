import styled from 'styled-components';
import { shade } from 'polished';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  height: 100%;
  background: #25274d;
`;

export const Form = styled.div`
  display: block;
  min-height: 400px;
  width: 450px;
  border-radius: 4px;
  margin: 60px 0;
  background: #464866;

  div {
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 40px 60px;

    h2 {
      align-self: flex-start;
      color: #edf5e1;
    }

    .email-input {
      align-self: flex-start;
      padding: 20px 20px 5px 0;

      input {
        width: 330px;
      }
    }

    .password-input {
      align-self: flex-start;
      padding: 15px 20px 30px 0;

      input {
        width: 330px;
      }
    }

    .signin-button {
      align-self: flex-start;
      width: 100%;
      height: 42px;
      font-size: 16px;
      font-weight: 700;
      background: #2e9cca;
      color: #edf5e1;

      &:hover {
        background: ${shade(0.1, '#2e9cca')};
      }

      &:hover {
        color: ${shade(0.1, '#edf5e1')};
      }
    }
  }
`;
