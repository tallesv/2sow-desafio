import styled from 'styled-components';
import { shade } from 'polished';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  margin: 20px 40px;
`;

export const FormContent = styled.div`
  display: block;
  min-height: 400px;
  width: 600px;
  border-radius: 4px;
  box-shadow: 0px 10px 10px #555;
  margin: 60px 0;

  div {
    display: flex;
    flex-direction: column;
    padding: 5px 8px;

    span {
      padding: 20px 0 20px 16px;
      color: gray;
      font-size: 13px;
    }

    label {
      padding-left: 18px;
    }

    div {
      .ui.pointing {
        width: 90%;
        align-self: center;
      }

      .cpf {
        max-width: 200px;
      }
      .cep {
        max-width: 200px;
      }

      .rua-numero {
        flex-direction: row;
        justify-content: space-between;
        padding: 5px 8px 5px 0;

        .rua {
          width: 100%;
          padding: 5px 8px 5px 0;
        }

        .numero {
          padding-left: 30px;

          input {
            max-width: 80px;
          }
        }
      }

      .form-buttons {
        flex-direction: row;
        justify-content: space-around;

        button {
          background: #2e9cca;
          color: #edf5e1;

          &:hover {
            background: ${shade(0.1, '#2e9cca')};
          }

          &:hover {
            color: ${shade(0.1, '#edf5e1')};
          }
        }
      }
    }
  }

  @media (max-width: 620px) {
    width: 450px;
  }
`;
