import React, { createContext, useCallback, useContext, useState } from 'react';
import { sign } from 'jsonwebtoken';
import api from '../services/api';

interface AuthState {
  token: string;
  user: User;
}

interface UserCredentials {
  email: string;
  senha: string;
}

interface User {
  nome: string;
  email: string;
  senha: string;
}

interface AuthContextData {
  user: User;
  signIn(credentials: UserCredentials): Promise<void | string>;
  signOut(): void;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

const AuthProvider: React.FC = ({ children }) => {
  const [data, setData] = useState<AuthState>(() => {
    const token = localStorage.getItem('@UserApp:token');
    const user = localStorage.getItem('@UserApp:user');

    if (token && user) {
      return { token, user: JSON.parse(user) };
    }

    return {} as AuthState;
  });

  const signIn = useCallback(async ({ email, senha }) => {
    const response = await api.get('/login');
    const users = response.data as User[];
    const findUser = users.find(user => user.email === email);
    if (!findUser) {
      return 'error';
    }
    if (findUser.senha !== senha) {
      return 'error';
    }
    const token = sign({}, '8c4f4fd85798aea6e6c63afe9465d82d', {
      subject: findUser.email,
      expiresIn: '1d',
    });
    localStorage.setItem('@UserApp:token', token);
    localStorage.setItem('@UserApp:user', JSON.stringify(findUser));

    setData({ token, user: findUser });
  }, []);

  const signOut = useCallback(() => {
    localStorage.removeItem('@UserApp:token');
    localStorage.removeItem('@UserApp:user');

    setData({} as AuthState);
  }, []);
  return (
    <AuthContext.Provider value={{ user: data.user, signIn, signOut }}>
      {children}
    </AuthContext.Provider>
  );
};

function useAuth(): AuthContextData {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth must be used within an AuthProvider');
  }

  return context;
}

export { AuthProvider, useAuth };
