import React from 'react';
import { Switch } from 'react-router-dom';
import Route from './Route';
import CreateUser from '../pages/CreateUser';
import EditUser from '../pages/EditUser';
import Dashboard from '../pages/Dashboard';
import SignIn from '../pages/SignIn';

const Routes: React.FC = () => (
  <Switch>
    <Route path="/signIn" component={SignIn} />
    <Route path="/createUser" component={CreateUser} isPrivate />
    <Route path="/editUser" component={EditUser} isPrivate />
    <Route path="/" component={Dashboard} exact isPrivate />
  </Switch>
);

export default Routes;
